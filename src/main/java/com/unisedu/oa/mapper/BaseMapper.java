package com.unisedu.oa.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BaseMapper<T> {

    T get(@Param("id") String id);

    T get(T t);

    List<T> findList(T t);

    List<T> findAll();

    List<T> findPage(T t, @Param("begin") int begin, @Param("end") int end, @Param("orderBy") String orderBy);

    T insert(T t);

    void update(T t);

    void delete(@Param("id") String id);

    void delete(T t);
}
