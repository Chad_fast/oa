package com.unisedu.oa.mapper;

import com.unisedu.oa.Entity.MenuEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MenuMapper extends BaseMapper<MenuEntity> {

    String getMenuByRole(String role);
}
