package com.unisedu.oa.controller;

import com.unisedu.oa.Entity.EmployeeEntity;
import com.unisedu.oa.service.EmployeeService;
import com.unisedu.oa.service.MenuService;
import com.unisedu.oa.utils.JsonUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class IndexController{


    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private MenuService menuService;



    /**
     * 首页显示数据
     * @return
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(HttpServletRequest request){
        JSONObject json = new JSONObject();

        //获取用户信息
        EmployeeEntity employeeEntity = employeeService.get(request);
        json.put("employee", JsonUtils.toJsonString(employeeEntity));

        if(employeeEntity==null){
            return employeeService.formatResult(403, "登录授权已过期，请重新登录", null);
        }else{
            //获取菜单信息
            JSONArray menus = menuService.getMenusByRole(employeeEntity.getRoleId());
            json.put("menu",menus);
        }
        json.put("messageCount",0);
        return employeeService.formatResult(200,null,json);
    }

    /**
     * 用户登录
     * @param request
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(HttpServletRequest request){
        String result = employeeService.login(request);
        return result;
    }


}
