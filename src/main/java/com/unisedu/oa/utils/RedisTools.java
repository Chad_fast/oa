package com.unisedu.oa.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class RedisTools {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 存储值
     * @param key
     * @param value
     */
    public void setString(String key, String value){
        stringRedisTemplate.opsForValue().set(key,value);
    }

    /**
     * 获取值
     * @param key
     * @return
     */
    public String getString(String key){
        return stringRedisTemplate.opsForValue().get(key)==null?"":stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 存储对象
     * @param key
     * @param obj
     */
    public void setObject(String key, Object obj){
        redisTemplate.opsForValue().set(key,obj);
    }

    /**
     * 获取对象
     * @param key
     * @return
     */
    public Object getObject(String key){
       return redisTemplate.opsForValue().get(key);
    }
}
