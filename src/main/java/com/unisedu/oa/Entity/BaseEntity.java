package com.unisedu.oa.Entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseEntity implements Serializable{

    public BaseEntity(){

    }

    public BaseEntity(String id) {
        this.id = id;
    }

    protected String id;
    protected String createTime;
    protected String createBy;
    protected String updateTime;
    protected String updateBy;
    protected String remarks;

}
