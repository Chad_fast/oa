package com.unisedu.oa.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class EmployeeEntity extends BaseEntity{

    public EmployeeEntity(){
        super();
    }

    private String name;
    private String loginName;
    @JsonIgnore
    private String password;
    private String sex;
    private String mobile;
    private String cardNumber;
    private String email;
    private String tel;
    private String probation;
    private String contract;
    private String confirmationTime;
    private String dept;
    private String company;
    private String status;

    /*关联属性*/
    private String roleId;


}
