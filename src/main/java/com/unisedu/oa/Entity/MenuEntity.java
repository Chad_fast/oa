package com.unisedu.oa.Entity;

import lombok.Data;

@Data
public class MenuEntity extends BaseEntity{

    private String name;
    private String level;
    private String href;
    private String status;
    private String parentId;
    private String icon;

}
