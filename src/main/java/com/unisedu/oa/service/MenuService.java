package com.unisedu.oa.service;

import com.unisedu.oa.Entity.MenuEntity;
import com.unisedu.oa.mapper.MenuMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class MenuService extends BaseService<MenuMapper,MenuEntity> {



    /**
     * 根据角色Id
     * @param roleId
     * @return
     */
    public JSONArray getMenusByRole(String roleId){
        String menu = redisTools.getString("MENU_"+roleId);
        if(StringUtils.isEmpty(menu)){
            menu = super.dao.getMenuByRole(roleId);
            redisTools.setString("MENU_"+roleId, menu);
        }
        JSONArray array = new JSONArray(menu);
        return array;
    }
}
