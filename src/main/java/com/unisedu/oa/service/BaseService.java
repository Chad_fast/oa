package com.unisedu.oa.service;

import com.unisedu.oa.mapper.BaseMapper;
import com.unisedu.oa.utils.JsonUtils;
import com.unisedu.oa.utils.RedisTools;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseService<D extends BaseMapper<T>, T> {

    @Autowired
    protected D dao;

    @Autowired
    protected RedisTools redisTools;

    /**
     * 根据Id获取对象
     *
     * @param id 主键
     * @return
     */
    public T get(String id) {
        return dao.get(id);
    }

    /**
     * 根据对象获取实例
     *
     * @param t 对象
     * @return
     */
    public T get(T t) {
        return dao.get(t);
    }

    /**
     * 返回响应结果
     *
     * @param code
     * @param message
     * @param data
     * @return
     */
    public String formatResult(Integer code, String message, Object data) {
        return JsonUtils.formatResult(code, message, data);
    }


}
