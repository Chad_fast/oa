package com.unisedu.oa;

import com.unisedu.oa.mapper.EmployeeMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@MapperScan("com.unisedu.oa.mapper")
@EnableAutoConfiguration
@SpringBootApplication
public class OaApplication{

	public static void main(String[] args) {
		SpringApplication.run(OaApplication.class, args);
	}

}
