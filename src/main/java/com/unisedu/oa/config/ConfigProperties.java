package com.unisedu.oa.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration
@ConfigurationProperties(prefix = "config",ignoreUnknownFields = false)
@PropertySource("classpath:config.properties")
@Data
@Component
public class ConfigProperties {

    private String secret;
}
